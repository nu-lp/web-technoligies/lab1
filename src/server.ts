import express, { NextFunction, Request, Response } from 'express';
import bodyParser from 'body-parser';
import config from './config';
import mongoose from 'mongoose';
import usersController from './user.controller';

const app = express();
const port = 3000;
const staticFilesDir = 'client';

/** Connection to MongoDb */
mongoose
    .connect(config.mongo.url)
    .then(() => {
        console.log('Successfully connected to Mongo');
    })
    .catch((error) => {
        console.error(error);
    });

/** Parse the body of the request */
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use((req: Request, res: Response, next: NextFunction) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    next();
});

app.use(express.static(staticFilesDir));

/** Routes */
app.get('/', (req: Request, res: Response) => {
    res.sendFile(`${staticFilesDir}/index.html`);
});
app.get('/users', usersController.getAllUsers);
app.post('/users', usersController.addUser);
app.put('/users', usersController.updateUser);
app.delete('/users', usersController.deleteUser);


/** Error handling */
app.use((req: Request, res: Response) => {
    const error = new Error('Not found');

    res.status(404).json({
        message: error.message
    });
});

app.listen(port, () => {
    console.log(`Server is listening on ${port}`);
});

console.log(`Starting server on port ${port}`);
