import mongoose, { Document, Schema } from 'mongoose';

export interface UserDto {
    id?: string;
    username: string;
    age: number;
}

interface User extends Document {
    username: string;
    age: number;
}

const UserSchema: Schema = new Schema(
    {
        username: {
            type: String,
            unique: true,
            required: true
        },
        age: {
            type: Number,
            required: true,
            min: 18,
            max: 70
        }
    },
    {
        versionKey: false
    }
);

export default mongoose.model<User>('User', UserSchema);
