import { Request, Response } from 'express';
import User, { UserDto } from './user.model';

const addUser = (req: Request, res: Response) => {
    const user = req.body as UserDto;
    const newUser = new User({
        username: user.username,
        age: user.age
    });

    return newUser.save()
        .then((saved) => {
            const response: UserDto = {
                id: saved.id,
                username: saved.username,
                age: saved.age
            };
            return res.status(201).json(response);
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error: error
            });
        });
};


const getAllUsers = (req: Request, res: Response) => {
    return User.find()
        .exec()
        .then((docs) => {
            const users: UserDto[] = docs.map(doc => {
                return {
                    id: doc.id,
                    username: doc.username,
                    age: doc.age
                };
            });
            return res.status(200).json(users);
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error: error
            });
        });
};

const updateUser = (req: Request, res: Response) => {
    const newUser = req.body as UserDto;
    const id = newUser.id;
    delete newUser.id;
    return User.findOneAndUpdate({_id: id}, newUser, {returnOriginal: false})
        .then((updated) => {
            const response: UserDto = {
                id: updated.id,
                username: updated.username,
                age: updated.age
            };
            return res.status(200).json(response);
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error: error
            });
        });
};


const deleteUser = (req: Request, res: Response) => {
    const {id} = req.body;
    return User.remove({_id: id})
        .then(() => {
            return res.status(204).json();
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error: error
            });
        });
};

export default {addUser, getAllUsers, updateUser, deleteUser};
