const apiUrl = 'http://localhost:3000';

function onRowUpdateBtnClick(user) {
    $('#user-id').val(user.id);
    $('#username').val(user.username);
    $('#age').val(user.age);
    $('#edit-btn').removeClass("d-none");
    $('#add-btn').addClass("d-none");
}

function createTable(table, users) {
    $(table).empty();
    if (users.length === 0) {
        const noUsersLabel = $('<td>').attr('colspan', 4).addClass('text-center h4 text-secondary').text('No users found');
        $('<tr>').append(noUsersLabel).appendTo($(table));
        return;
    }
    for (let i = 0; i < users.length; i++) {
        const row = $('<tr>');
        $('<td>').text(i + 1).appendTo(row);
        $('<td>').text(users[i].username).appendTo(row);
        $('<td>').text(users[i].age).appendTo(row);

        const actionsCol = $('<td>').appendTo(row);
        $('<button>').addClass('btn btn-warning me-2')
            .html('<i class="bi bi-pencil"></i> Edit')
            .click(function () {
                onRowUpdateBtnClick(users[i])
            })
            .appendTo(actionsCol);
        $('<button>').addClass('btn btn-danger')
            .html('<i class="bi bi-x-lg"></i> Delete')
            .click(function () {
                deleteUser(users[i].id);
            })
            .appendTo(actionsCol);

        $(table).append(row);
    }
}

function getUsers() {
    $.get(`${apiUrl}/users`, function (users) {
        console.log('received users', users)
        createTable($('#users-table-body'), users);
    });
}

function addUser(user) {
    $.post(`${apiUrl}/users`, user, function (data) {
        console.log('created user', data);
        getUsers();
    })
}

function updateUser(user) {
    $.ajax({
        url: `${apiUrl}/users`,
        data: user,
        type: 'PUT',
        success: function (data) {
            console.log('updated user', data)
            getUsers();
        }
    })
}

function deleteUser(id) {
    $.ajax({
        url: `${apiUrl}/users`,
        data: {
            id: id
        },
        type: 'DELETE',
        success: function () {
            console.log('user deleted');
            getUsers();
        }
    });
}

function clearForm() {
    $(':input', $('#user-data-form')).not(':button, :submit, :reset').val('');
}

$(document).ready(function () {
    // Form submit handler
    $('#user-data-form').submit(function (event) {
        addUser($('#user-data-form').serialize());
        clearForm();
        event.preventDefault();
    });
    // Update user button handler
    $('#edit-btn').click(function () {
        updateUser($('#user-data-form').serialize());
        clearForm();
        $('#edit-btn').addClass("d-none");
        $('#add-btn').removeClass("d-none");
    });

    getUsers();
});
